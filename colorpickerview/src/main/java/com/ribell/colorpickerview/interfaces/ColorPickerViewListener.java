package com.ribell.colorpickerview.interfaces;

/**
 * Created by ferranribell on 19/08/15.
 */
public interface ColorPickerViewListener {
    void onColorPickerClick(int color);
}
